package model;

import target.Recharge;

public class Xiaomi extends Phone implements Recharge {
    private final Recharge recharge;
    public Xiaomi(String name, Recharge recharge) {
        super(name);
        this.recharge = recharge;
    }

    @Override
    public void recharge() {
        this.recharge.recharge();
    }

    @Override
    public void useType() {
        this.recharge.useType();
    }
}
