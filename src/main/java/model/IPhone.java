package model;

import adaptee.ILightning;

public class IPhone extends Phone implements ILightning {
    public IPhone(String name) {
        super(name);
    }

    @Override
    public void recharge() {
        System.out.println("Lightning is connection!");
    }

    @Override
    public void useLightning() {
        System.out.println("Recharge started - Lightning");
        System.out.println("Recharge finished - Lightning");
    }
}
