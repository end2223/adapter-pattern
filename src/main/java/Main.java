import adaptee.ILightning;
import adapter.LightningToMicroUSBAdapter;
import adapter.LightningUSBToTypeCAdapter;
import model.IPhone;
import model.Xiaomi;
import target.TypeC;
import target.Recharge;
/*
* So do em ve luc dau sai o cho khai bao doi tuong Adapter
* -----
* Target: Recharge(Android)[Type C, Micro USB]
* Adaptee: ILightning(Iphone)
*
* */
public class Main {
    public static void main(String[] args) {
        /*
        * Dien thoai Android co 2 loai sac
        * Type C
        * Micro USB
        * */

        Xiaomi xiaomi = new Xiaomi("Mi A3", new TypeC());
        IPhone iPhone = new IPhone("Iphone 13");

        System.out.println();
        System.out.println("------------");
        System.out.println("Phone name: " + xiaomi.getName());
        System.out.println("Android with Android type!");
        rechargeMicroUsbPhone(xiaomi);

        System.out.println();
        System.out.println("------------");
        System.out.println("Phone name: " + iPhone.getName());
        System.out.println("Lightning with Lightning  type!");
        rechargeLightningPhone(iPhone);

        /*
        * Lightning to Type C
        * */
        System.out.println();
        System.out.println("------------");
        System.out.println("Phone name: " + iPhone.getName());
        System.out.println("Lightning to Android Type");
        rechargeMicroUsbPhone(new LightningToMicroUSBAdapter(iPhone));

        /*
        * Lightning to Micro USB
        * */
        System.out.println();
        System.out.println("------------");
        System.out.println("Phone name: " + iPhone.getName());
        System.out.println("Lightning to Android Type");
        rechargeMicroUsbPhone(new LightningUSBToTypeCAdapter(iPhone));
    }

    public static void rechargeLightningPhone(ILightning phone) {
        phone.useLightning();
        phone.recharge();
    }

    public static void rechargeMicroUsbPhone(Recharge phone){
        phone.useType();
        phone.recharge();
    }
}
