package adapter;

import adaptee.ILightning;
import target.MicroUSB;

public class LightningUSBToTypeCAdapter extends MicroUSB {
    private final ILightning iLightning;

    public LightningUSBToTypeCAdapter(ILightning iLightning) {
        this.iLightning = iLightning;
    }

    @Override
    public void useType() {
        //process logic to converting
        System.out.println("Use Type C");
        this.iLightning.useLightning();
    }


    @Override
    public void recharge() {
        //process logic to converting
        iLightning.recharge();
    }
}
