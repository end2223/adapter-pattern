package adapter;

import adaptee.ILightning;
import target.MicroUSB;

public class LightningToMicroUSBAdapter extends MicroUSB {
    private final ILightning iLightning;

    public LightningToMicroUSBAdapter(ILightning iLightning) {
        this.iLightning = iLightning;
    }

    @Override
    public void useType() {
        //process logic to converting
        System.out.println("Use Micro Usb");
        this.iLightning.useLightning();
    }

    @Override
    public void recharge() {
        //process logic to converting
        iLightning.recharge();
    }
}
