package adaptee;

public interface ILightning {
    void recharge();
    void useLightning();
}
