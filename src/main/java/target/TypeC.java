package target;

public class TypeC implements Recharge{
    @Override
    public void recharge() {
        System.out.println("Type C is connection!");
    }

    @Override
    public void useType() {
        System.out.println("Recharge started - Type C");
        System.out.println("Recharge finished - Type C");
    }
}
